#!/usr/bin/env bash

### Permite que funções sejam chamadas via piping (STDIN) ou via parâmetros
### Somente funciona no BASH 4!
### O tempo 0.01 depende da velocidade do clock e outras variáveis. 0.01 é um bom meio-termo.
function funcPipeAndEcho () {
    if read -t 0.01 STDIN; then
        input="${STDIN}"
    else
        input="$*"
    fi
    echo "$input"
}



### Direciona todo STDOUT e STDERR a um arquivo temporário
### No final (ou se acontecer um erro) cospe uma janela do zenity para debug
tmpfile=$(mktemp)
exec > $tmpfile
exec 2>&1
function debug_std () {
    # Filtra mensagens de erros do GTK
    cat $tmpfile | grep -v 'Gtk-' | grep -v '^$' | zenity --text-info --title="Log Output"
}
trap debug_std QUIT INT TERM EXIT



### Verifica se já existe uma instância rodando
## One liner
[[ "$(pgrep -fn $0)" -ne "$(pgrep -fo $0)" ]] && echo "At least 2 copies of $0 are running." && exit

## Com lockfiles (mkdir é atomico, é o melhor jeito)
LOCKDIR="/tmp/$(basename $0)"
if ! mkdir ${LOCKDIR} 2>/dev/null; then
    echo "$LOCKDIR exists so there's another instance running." >&2
    exit 1
fi
# Não esquecer de trapear as saídas por se o script terminar prematuramente
trap "rm -rf ${LOCKDIR}" QUIT INT TERM EXIT



### Roda o script em baixas prioridades
ionice -c 3 -p $$ 1>/dev/null
renice +12  -p $$ 1>/dev/null



### Captura a área de transferência dando prioridade à seleção primária
## Assim não é necessário - porém possível - apertar CTRL+C antes de rodar o script
_primary=$(xclip -o -selection primary)
_clipboard=$(xclip -o -selection clipboard)
if   [[ "$_primary" =~ ^https*://.*ebay\.com.+$ ]]; then
    _url="$_primary"
elif [[ "$_clipboard" =~ ^https*://.*ebay\.com.+$ ]]; then
    _url="$_clipboard"
else
    echo "URL is not an eBay link!"
fi